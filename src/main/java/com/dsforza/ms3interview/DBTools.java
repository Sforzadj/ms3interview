package com.dsforza.ms3interview;

import com.dsforza.ms3interview.DataModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * @author Dan
 * DBTools Class
 * <p>
 * DBTools handles sinking data into a database
 * 
 */
public class DBTools {
	
	private final String dbURL = "jdbc:sqlite:src/main/resources/MS3.db";
	
	/**
	 * DBTools
	 * <p>
	 * Default Constructor - No parameters required
	 */
	public DBTools() {
		this.createTable();
	}
	
	/**
	 * createTable
	 * <p>
	 * DDL - Create a SQL table using defined column headers and type with restraints
	 */
	private void createTable() {
		String sql = "CREATE TABLE IF NOT EXISTS MS3Dataset (\n"
				+ "A text NOT NULL, \n"
				+ " B text NOT NULL, \n"
				+ " C text NOT Null, \n"
				+ " D text NOT NULL, \n"
				+ " E text NOT NULL, \n"
				+ " F text NOT NULL, \n"
				+ " G real NOT NULL, \n"
				+ " H int, \n"
				+ " I int, \n"
				+ " J text NOT NULL\n"
				+ ");";

		this.executeSQL(sql);
	}
	
	/**
	 * executeSQL
	 * <p>
	 * Creates a connection to the database, then executes a statement to create a table
	 * 
	 * @param sql
	 * 
	 */
	private void executeSQL(String sql) {
		try (Connection conn = DriverManager.getConnection(this.dbURL);
				Statement stmt = conn.createStatement()) {
			stmt.execute(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/**insertRow
	 * <p>
	 * Inserts a row of data (type = ArrayList<Object>) into the database
	 * 
	 * @param dm
	 */
	public void insertRow(DataModel dm)	{

		String sql = "INSERT INTO MS3Dataset('A','B','C','D','E','F','G','H','I','J') VALUES(?,?,?,?,?,?,?,?,?,?)";
		ArrayList<Object> row = dm.getDMArray();
		
		try (Connection conn = DriverManager.getConnection(this.dbURL);
				PreparedStatement pstmt = conn.prepareStatement(sql)) {

			for(int i = 1; i <= row.size(); i++) {
				pstmt.setObject(i, row.get(i-1));
			}
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

}
