package com.dsforza.ms3interview;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

import com.dsforza.ms3interview.Importer;
import com.dsforza.ms3interview.DBTools;

public class App 
{
    public static void main( String[] args ) throws IOException {
        
    	//Timestamp for output
    	String timeStamp = new SimpleDateFormat("yyyy.MM.dd-HH.mm.ss").format(new java.util.Date());
    	
    	//New File object for the csv
    	File file = new File("src/main/resources/ms3Interview.csv");
           	
        //Import data using Importer object
        Importer data = new Importer(file);
        
        //Establish DB connection
        DBTools db = new DBTools();
        
        //Counters for the final log statistics
        int recordsReceived = 0;
        int recordsSuccessful = 0;
        
        for (CSVRecord csvRecord : data.getParser()) {
        	//Validation 1 and 2 - Before conversion
        	if (data.isValid(csvRecord)){
        		
        		//Create Model object
        		DataModel goodRecord = new DataModel(csvRecord);
        		
        		//Validation 3 - Once the DataModel Object was created
        		if(goodRecord.hasConversionErrors()) { 
        			continue;
        		}
        		
        		//Insert the row
        		db.insertRow(goodRecord);
        		recordsReceived++;
        		recordsSuccessful++;
        		
        	} else {
		    	recordsReceived++;
        		
        		//Convert csvRecord to a String without the Mapping values
        		String values = new String();
        		for (int i = 0; i < csvRecord.size(); i++){
        			values += csvRecord.get(i) + ",";
        		}
        		
        		//Write it to the Bad-Data log
		    	 try {
		    		 FileUtils.writeStringToFile(new File("src/main/resources/Bad-Data-"+ timeStamp +".csv"), values+"\n",  Charset.defaultCharset(), true);
		    	 } catch (IOException e) {
		    		 e.printStackTrace();
		    	 }
		     }
        }
        data.closeParser();
        
        System.out.println("The import has completed! \n The log file can be found in src/main/resources/log.txt");
        File log = new File("src/main/resources/log.txt");
        
        //Concatenate Statistics for log report
		String stats = "Records Received: " + recordsReceived + "\n"
				+ "Records Successfully Imported: " + recordsSuccessful +"\n"
				+ "Records Failed: " + (recordsReceived - recordsSuccessful) + "";
				
		//Write the log file
        try {
			FileUtils.writeStringToFile(log, stats, Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
		}
        
    
        
    }
    
}
