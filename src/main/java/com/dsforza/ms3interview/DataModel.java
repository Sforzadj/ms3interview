package com.dsforza.ms3interview;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.csv.CSVRecord;

class DataModel {

	//Class Fields
	private CSVRecord csvRecord;
	private ArrayList<Object> dmArray;
	private String a;
	private String b;
	private String c;
	private String d;
	private String e;
	private String f;
	private double g;
	private int h;
	private int i;
	private String j;
	
	/**
	 * Constructor for DataModel
	 * @param csvRecord
	 */
	public DataModel(CSVRecord csvRecord) {
	
		this.csvRecord = csvRecord;
		this.dmArray = new ArrayList<Object>();
		
		//consider making this a class field - it's used every time
		ArrayList<String> convertedVals = convert(csvRecord);
		
		setA(convertedVals.get(1));
		setB(convertedVals.get(2));
		setC(convertedVals.get(3));
		setD(convertedVals.get(4));
		setE(convertedVals.get(5));
		setF(convertedVals.get(6));
		setG(convertedVals.get(7));
		setH(convertedVals.get(8));
		setI(convertedVals.get(9));
		setJ(convertedVals.get(10));
		
		this.populateDMArray();
	}
	
	/**
	 * PopulateDMArray
	 * <p>
	 * Populates the DataModel Array using DataModel getters
	 */
	private void populateDMArray() {
	
		dmArray.add(getA());
		dmArray.add(getB());
		dmArray.add(getC());
		dmArray.add(getD());
		dmArray.add(getE());
		dmArray.add(getF());
		dmArray.add(getG());
		dmArray.add(getH());
		dmArray.add(getI());
		dmArray.add(getJ());
	}
	
	/**
	 * convert
	 * <p>
	 * Converts a CSVRecord to an ArrayList<String>
	 * 
	 * @param csvRecord
	 * @return ArrayList<String>
	 */
	private ArrayList<String> convert(CSVRecord csvRecord) {

		Iterator<String> e = csvRecord.iterator();
		ArrayList<String> tempConvertedVals = new ArrayList<String>();
		
		//WORKAROUND - ArrayList.get doesn't like 0 as an index. For now, append a blank space. Come back to me!
		tempConvertedVals.add(" ");
		
		while(e.hasNext()){
			tempConvertedVals.add(e.next().toString());			
		}
		return tempConvertedVals;
	}
	
	/**
	 * stringToDouble
	 * <p>
	 * Takes in a string containing a double and casts it to a double
	 * 
	 * @param String val
	 * @return Double
	 */
	private double stringToDouble(String val) {
		val = val.substring(1);
		double g = Double.parseDouble(val);
		return g;
	}
	
	/**
	 * stringToInt
	 * <p>
	 * Takes in a string containing an int and casts it to type int
	 * Because SQLite does not have a primitive type for Boolean, 
	 * they must be represented by 1 = true and 0 = false
	 * @param val
	 * @return int representation of Boolean
	 */
	private int stringToInt(String val) {
		if(val.toUpperCase() == "TRUE") {
			return 1;
		} else {
			return 0;
		}
	}
	
	//Validation 3 - Check value of G for -52787 (Value threw NumericalFormatException)
	/**
	 * hasConversionErrors
	 * <p>
	 * Some columns in the data set have data of the wrong type, which can not easily be cast.
	 * If a non-castable value is found, the value for that cell is set to -52787 in the setter.
	 * 
	 * @return Boolean
	 */
	public boolean hasConversionErrors() {
		if (this.getG() == -52787.00) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * setCSVRecord
	 * @param csvRecord
	 */
	public void setCSVRecord(CSVRecord csvRecord) {
		this.csvRecord = csvRecord;
	}
	
	/**
	 * getCSVRecord
	 * @return CSVRecord
	 */
	public CSVRecord getCSVRecord() {
		return this.csvRecord;
	}
	
	/**
	 * getDMArray
	 * @return ArrayList<Object> 
	 */
	public ArrayList<Object> getDMArray() {
		return this.dmArray;
	}
	
	/**
	 * setA
	 * @param a
	 */
	public void setA(String a) {
		this.a = a;
	}
	
	/**
	 * getA
	 * @return String
	 */
	public String getA() {
		return this.a;
	}
	
	/**
	 * setB
	 * @param b
	 */
	public void setB(String b) {
		this.b = b;
	}
	
	/**
	 * getB
	 * @return String
	 */
	public String getB() {
		return this.b;
	}
	
	/**
	 * setC
	 * @param c
	 */
	public void setC(String c) {
		this.c=c;
	}
	
	/**
	 * getC
	 * @return String
	 */
	public String getC() {
		return this.c;
	}
	
	/**
	 * setD
	 * @param d
	 */
	public void setD(String d) {
		this.d=d;
	}
	
	/**
	 * getD
	 * @return String
	 */
	public String getD() {
		return this.d;
	}
	
	/**
	 * setE
	 * @param e
	 */
	public void setE(String e) {
		this.e=e;
	}
	
	/**
	 * getE
	 * @return String
	 */
	public String getE() {
		return this.e;
	}
	
	/**
	 * setF
	 * @param f
	 */
	public void setF(String f) {
		this.f=f;
	}
	
	/**
	 * getF
	 * @return String
	 */
	public String getF() {
		return this.f;
	}
	
	/**
	 * setG
	 * <p>
	 * If the value of column G is not able to be cast to a double, set the value to -52787 
	 * @param g
	 */
	public void setG(String g) {
		try {
			this.g=stringToDouble(g);
		} catch (NumberFormatException e){
			this.g = -52787;
		}
		
	}
	/**
	 * getG
	 * @return
	 */
	public double getG() {
		return this.g;
	}
	
	/**
	 * setH
	 * <p>
	 * Converts the value of H from a string to an int, via DataModel.stringToInt
	 * @param h
	 */
	public void setH(String h) {
		this.h=stringToInt(h);
	}
	
	/**
	 * getH
	 * @return int
	 */
	public int getH() {
		return this.h;
	}
	
	/**
	 * setI
	 * <p>
	 * Converts the value of I from a string to an int, via DataModel.stringToInt
	 * @param i
	 */
	public void setI(String i) {
		this.i=stringToInt(i);
	}
	
	/**
	 * getI
	 * @return int
	 */
	public int getI() {
		return this.i;
	}
	/**
	 * setJ
	 * @param j
	 */
	public void setJ(String j) {
		this.j=j;
	}
	
	/**
	 * getJ
	 * @return String
	 */
	public String getJ() {
		return this.j;
	}
}
	