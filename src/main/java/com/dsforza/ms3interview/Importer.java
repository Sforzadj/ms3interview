package com.dsforza.ms3interview;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileReader;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 * @author Dan
 *Importer Class
 *<p>
 *Reads in CSV files and performs validations
 *
 */
public class Importer {
	
	private File file;
	private CSVParser parser;
	
	/**
	 * Importer Constructor
	 * @param file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public Importer(File file) throws FileNotFoundException, IOException {
		this.file = file;
		this.parser = importFile(file);
	}
	
	/**
	 * closeParser
	 * <p>
	 * Closes the parser from the Importer Object
	 * @throws IOException
	 */
	public void closeParser() throws IOException {
		this.parser.close();
	}
		
	/**
	 * importFile
	 * <p>
	 * Opens the file and returns a parser object for the file. Make sure to close the parser when complete.
	 * @param file
	 * @return CSVParser Object
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private CSVParser importFile(File file) throws FileNotFoundException, IOException {
		CSVParser parser = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(new FileReader(file));
		return parser;
	}
	
	/**
	 * isValid
	 * <p>
	 * Validates CSVRecords for conformity to DB schema
	 * @param csvRecord
	 * @return boolean
	 */
	public boolean isValid(CSVRecord csvRecord) {
		if (csvRecord.isConsistent()){
			if (noEmptyStrings(csvRecord)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * noEmptyStrings
	 *<p>
	 *Check for empty strings within row
	 *If a row has NO empty cells, return true
	 * @param r
	 * @return boolean
	 */
	private boolean noEmptyStrings(CSVRecord r) {
		for (int i = 0; i< r.size(); i++) {
			if(r.get(i).equals("")) {
				return false;
			} else {
				continue;
			}
		}
		return true;
	}
	
	/**
	 * setParser
	 * @param parser
	 */
	public void setParser(CSVParser parser) {
		this.parser = parser;
	}
	
	/**
	 * getParser
	 * @return CSVParser Object
	 */
	public CSVParser getParser() {
		return this.parser;
	}
	
	/**
	 * setFile
	 * @param file
	 */
	public void setFile(File file) {
		this.file = file;
	}
	
	/**
	 * getFile
	 * @return File Object
	 */
	public File getFile() {
		return this.file;
	}
}
